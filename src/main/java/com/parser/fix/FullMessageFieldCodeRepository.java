package com.parser.fix;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository
public class FullMessageFieldCodeRepository {

	public Map<Integer, String> getFieldCodesShouldBePrinted() {
		Map<Integer, String> fieldCodesShouldBePrinted = new HashMap<>();
		fieldCodesShouldBePrinted.put(55, "Symbol");
		fieldCodesShouldBePrinted.put(270, "Buy And Sell Price");
		fieldCodesShouldBePrinted.put(271, "Buy And Sell Amount");
		fieldCodesShouldBePrinted.put(52, "Time and Date");
		return fieldCodesShouldBePrinted;
	}

}
