package com.parser.fix;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

@Service
public class FixMessageService {

	private String consoleResultSeperator = ": ";
	private String consoleMessageHeader = "Message Information is below: ";

	@Autowired
	FullMessageFieldCodeRepository fullMessageFieldCodeRepository;

	public MessageParserResult processParsingFullMessageBody(String fullMessageBody) {

		String[] messages = fullMessageBody.split(FixMessageValidator.MESSAGE_SEPERATOR);

		return processMessage(messages);
	}

	private MessageParserResult processMessage(String[] messages) {
		List<Multimap<Integer, String>> validMessages = new ArrayList<>();
		List<String> invalidMessages = new ArrayList<>();
		for (String message : messages) {
			if (message == null || message.isEmpty()) {
				continue;
			}
			if (FixMessageValidator.isSingleMessageValid(message)) {
				validMessages.add(parseSingleMessages(message));
			} else {
				invalidMessages.add(message);
			}
		}
		printValidMessages(validMessages);

		return new MessageParserResult(validMessages.size(), invalidMessages.size());
	}

	private void printValidMessages(List<Multimap<Integer, String>> validMessages) {
		validMessages.forEach(this::printValidMessagesToConsole);
	}

	private void printValidMessagesToConsole(Multimap<Integer, String> validMessage) {
		System.out.println("\n" + consoleMessageHeader);
		Map<Integer, String> fieldsShouldBePrinted = fullMessageFieldCodeRepository.getFieldCodesShouldBePrinted();
		fieldsShouldBePrinted.entrySet().stream().forEach(field -> printField(validMessage, field));
	}

	private void printField(Multimap<Integer, String> validMessage, Map.Entry<Integer, String> field) {
		System.out.println(
				field.getValue() + consoleResultSeperator + StringUtils.join(validMessage.get(field.getKey()), '&'));
	}

	private Multimap<Integer, String> parseSingleMessages(String singleMessageBody) {
		Multimap<Integer, String> messageMap = ArrayListMultimap.create();
		String[] messageFields = singleMessageBody.split(FixMessageValidator.FIELD_SEPERATOR);
		for (String field : messageFields) {
			if (!field.contains("=")) {
				continue;
			}
			String[] keyAndValue = field.split("=");
			messageMap.put(Integer.parseInt(keyAndValue[0]), keyAndValue[1]);
		}
		return messageMap;
	}
}
