package com.parser.fix;

import java.io.File;
import java.util.Scanner;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/parser")
public class FixMessageController {
	Log logger = LogFactory.getLog(getClass());

	private String invalidMessageException = "Message is invalid";
	private String validMessageCountInformationText = "Count of valid messages is: ";
	private String invalidMessageCountInformationText = "Count of invalid messages is: ";

	@Autowired
	FixMessageService fixMessageService;

	@ResponseBody
	@RequestMapping(value = "/fullrefreshmessage", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
	public void parseFullRefreshMessage(@RequestBody String fullMessageBody) {
		if (!FixMessageValidator.isResponseMessageValid(fullMessageBody)) {
			throw new IllegalArgumentException(invalidMessageException);
		}
		MessageParserResult result = fixMessageService.processParsingFullMessageBody(fullMessageBody);
		printProcessedMessageCountsToConsole(result);

		ResponseEntity.ok(result);
	}

	@ResponseBody
	@RequestMapping(value = "/fullrefreshmessage/fromfile", method = RequestMethod.POST)
	public void parseFullRefreshMessageFromFile() {
		String fullMessageBody = readMessagesFromFile();

		if (!FixMessageValidator.isResponseMessageValid(fullMessageBody)) {
			throw new IllegalArgumentException(invalidMessageException);
		}

		MessageParserResult result = fixMessageService.processParsingFullMessageBody(fullMessageBody);
		printProcessedMessageCountsToConsole(result);

		ResponseEntity.ok(result);
	}

	private String readMessagesFromFile() {
		String fullMessageBody = null;
		Scanner scanner = null;
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource("messages.txt").getFile());
			scanner = new Scanner(file);
			fullMessageBody = scanner.useDelimiter("\\Z").next();

		} catch (Exception e) {
			logger.error(e);
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
		return fullMessageBody;
	}

	private void printProcessedMessageCountsToConsole(MessageParserResult result) {
		System.out.println("\n" + validMessageCountInformationText + result.getCountOfValidMessages());
		System.out.println("\n" + invalidMessageCountInformationText + result.getCountOfInvalidMessages());
	}
}
