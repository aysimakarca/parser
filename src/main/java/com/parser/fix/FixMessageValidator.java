package com.parser.fix;

public class FixMessageValidator {
	private static final String CHECKSUM_BEGIN_STRING = "10=";
	public static final String MESSAGE_SEPERATOR = "8=FIX";
	public static final String FIELD_SEPERATOR = "\1";

	private FixMessageValidator() {
		throw new IllegalStateException();
	}

	static boolean isResponseMessageValid(String messageBody) {
		boolean isValid = true;

		if (messageBody == null || messageBody.isEmpty()) {
			return false;
		}
		if (!messageBody.contains(MESSAGE_SEPERATOR)) {
			return false;
		}

		return isValid;
	}

	static boolean isSingleMessageValid(String singleMessageBody) {
		if (singleMessageBody == null || singleMessageBody.isEmpty()) {
			return false;
		}

		return isCheckSumValid(singleMessageBody);
	}

	private static boolean isCheckSumValid(String singleMessageBody) {
		if (!singleMessageBody.contains(CHECKSUM_BEGIN_STRING)) {
			return false;
		}
		String[] seperatedByCheckSum = (MESSAGE_SEPERATOR + singleMessageBody).split(CHECKSUM_BEGIN_STRING);
		byte[] byteArrayOfMessage = seperatedByCheckSum[0].getBytes();
		byte checkSumOfMessage = 0;
		for (byte byteItem : byteArrayOfMessage) {
			checkSumOfMessage += byteItem;
		}
		int calculatedCheckSumOfMessage = checkSumOfMessage & 0xFF;
		return calculatedCheckSumOfMessage == getCheckSumInMessage(seperatedByCheckSum);
	}

	private static Integer getCheckSumInMessage(String[] seperatedByCheckSum) {
		String checkSumInMessage = seperatedByCheckSum[1].contains(FIELD_SEPERATOR)
				? seperatedByCheckSum[1].split(FIELD_SEPERATOR)[0]
				: seperatedByCheckSum[1];

		return Integer.parseInt(checkSumInMessage);
	}
}
