package com.parser.fix;

public class MessageParserResult {

	private Integer countOfValidMessages;
	private Integer countOfInvalidMessages;

	public MessageParserResult(Integer countOfValidMessages, Integer countOfInvalidMessages) {
		this.countOfInvalidMessages = countOfInvalidMessages;
		this.countOfValidMessages = countOfValidMessages;
	}

	public Integer getCountOfValidMessages() {
		return countOfValidMessages;
	}

	public void setCountOfValidMessages(Integer countOfValidMessages) {
		this.countOfValidMessages = countOfValidMessages;
	}

	public Integer getCountOfInvalidMessages() {
		return countOfInvalidMessages;
	}

	public void setCountOfInvalidMessages(Integer countOfInvalidMessages) {
		this.countOfInvalidMessages = countOfInvalidMessages;
	}

}
