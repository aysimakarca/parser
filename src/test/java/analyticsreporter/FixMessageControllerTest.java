package analyticsreporter;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.parser.WebApplication;
import com.parser.fix.FixMessageController;
import com.parser.fix.FixMessageService;
import com.parser.fix.MessageParserResult;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class)
@WebAppConfiguration
public class FixMessageControllerTest {

	@Mock
	private FixMessageService fixMessageService;

	@InjectMocks
	FixMessageController fixMessageController;

	private MockMvc mockMvc;

	private MessageParserResult messageParserResult;

	private String mockedMessage = "8=FIX.4.49=14235=W34=049=justtech52=20180206-21:43:36.00056=user262=TEST55=EURUSD268=2269=0270=1.31678271=100000.0269=1270=1.31667271=100000.010=057";

	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(fixMessageController).build();
		messageParserResult = mock(MessageParserResult.class);
		messageParserResult.setCountOfInvalidMessages(1);
		messageParserResult.setCountOfValidMessages(1);
		when(fixMessageService.processParsingFullMessageBody(any())).thenReturn(messageParserResult);
	}

	@Test
	public void testParseFullRefreshMessageFromFileAndExpectStatatusOk() throws Exception {
		mockMvc.perform(post("/parser/fullrefreshmessage/fromfile").contentType(MediaType.ALL))
				.andExpect(status().isOk());
	}

	@Test
	public void testParseFullRefreshMessageWithValidMessageAndExpectStatusOk() throws Exception {
		mockMvc.perform(
				post("/parser/fullrefreshmessage").content(mockedMessage).contentType(MediaType.TEXT_PLAIN_VALUE))
				.andExpect(status().isOk());
	}

	@Test
	public void testParseFullRefreshMessageWithEmptyMessageAndExpectClientError() throws Exception {
		mockMvc.perform(post("/parser/fullrefreshmessage").content("").contentType(MediaType.TEXT_PLAIN_VALUE))
				.andExpect(status().is4xxClientError());

	}

}
