package analyticsreporter;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.parser.WebApplication;
import com.parser.fix.FixMessageService;
import com.parser.fix.MessageParserResult;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class)
@WebAppConfiguration
public class FixMessageServiceTest {

	private String mockedValidMessage = "8=FIX.4.49=14235=W34=049=justtech52=20180206-21:43:36.00056=user262=TEST55=EURUSD268=2269=0270=1.31678271=100000.0269=1270=1.31667271=100000.010=057";
	private String mockedInvalidMessage = "8=FIX.4.49=14235";

	@Autowired
	private FixMessageService fixMessageService;

	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testProcessParsingFullMessageBody() {
		MessageParserResult result = fixMessageService.processParsingFullMessageBody("");
		assertNotNull(result);
	}

	@Test
	public void testProcessParsingFullMessageBodyWithOneValidAndSeeCountOfValidMessageIsOne() {
		MessageParserResult result = fixMessageService.processParsingFullMessageBody(mockedValidMessage);
		assert (result.getCountOfValidMessages().equals(1));
	}

	@Test
	public void testProcessParsingFullMessageBodyWithOneInvalidAndSeeCountOfInvalidMessageIsOne() {
		MessageParserResult result = fixMessageService.processParsingFullMessageBody(mockedInvalidMessage);
		assert (result.getCountOfInvalidMessages().equals(1));
	}

	@Test
	public void testProcessParsingFullMessageBodyWithOneValidOneInvalidMessageAndSeeCountOfValidMessagesAreCorrect() {
		MessageParserResult result = fixMessageService
				.processParsingFullMessageBody(mockedValidMessage + mockedInvalidMessage);
		assert (result.getCountOfInvalidMessages().equals(1) && result.getCountOfValidMessages().equals(1));
	}

}
